## Roles en la formación {#roles-en-la-formaci-n}

Doceo recoge diferentes posibles roles para cada usuario en función del papel que desempeñe ese docente en la formación del profesorado.

En concreto los posibles roles son:

* Superadministración
* Supervisión
* Dirección de Centro de Profesorado
* Secretaría de Centro de Profesorado
* Asesoría de Centro de Profesorado
* Dirección de Centro Educativo
* Secretaría de Centro Educativo
* Jefatura de Estudios de Centro Educativo
* Coordinación de la Formación (COFO)
* Coordinación de la Formación en las Tecnologías del Aprendizaje y el Conocimiento (CofoTap)
* Coordinación de convivencia
* Coordinación de igualdad
* Docente
* Usuario general

Cada rol tiene diferentes permisos y puede acceder a diferentes partes de la aplicación debido a su papel en la formación del profesorado.

Es posible que un mismo usuario tenga dos roles diferentes, por ejemplo, un director es a la vez docente.

Sin embargo en cada momento Para consultar tu Rol y para cambiar de Rol en la página de cambio de ROL.

Se accede pulsando en este icono:

![](https://raw.githubusercontent.com/catedu/manualdoceo/master/assets/cambiarrol.png)

Y luego se puede cambiar de rol activo y configurar el rol preferente.

![](https://raw.githubusercontent.com/catedu/manualdoceo/master/assets/cambiarrolactivo.png)
